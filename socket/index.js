import * as config from "./config";
import { texts } from "./../data";
let users = [];
let gameTimer;

const getResults = users=>{
  return users.sort((a, b) =>{
    return b.progress - a.progress;
}).map((user, index)=>`${index+1}. ${user.name}-${user.progress}%`).join("<br/>");
}

const formatTime = time => {
  let minutes = parseInt(time / 60, 10);
  let seconds = parseInt(time % 60, 10);

  minutes = minutes < 10 ? "0" + minutes : minutes;
  seconds = seconds < 10 ? "0" + seconds : seconds;

  return `${minutes}:${seconds}`;
}

const startGame = (socket) => {
    const randomIndex = Math.floor(Math.random() * texts.length);
    const text = texts[randomIndex];

    let timeLeft = config.SECONDS_FOR_GAME;
      gameTimer= setInterval(()=>{
        socket.emit('UPDATE_GAME_TIMER', formatTime(timeLeft));
        socket.broadcast.emit('UPDATE_GAME_TIMER', formatTime(timeLeft));
        timeLeft--;
        if (timeLeft === -1) {
          clearInterval(gameTimer);
          const results = getResults(users);
          socket.broadcast.emit("GAME_OVER_DONE", results);
          socket.emit("GAME_OVER_DONE", results);

        }
      }, 1000);
      socket.emit("SHOW_TEXT", text);
      socket.broadcast.emit("SHOW_TEXT", text);
  }

export default io => {
    io.on("connection", socket => {
            const username = socket.handshake.query.username;

            // activate user if username is unique
            if (users.find(user=>user.name===username)) {
                socket.emit("DEACTIVATE_USER");
                socket.disconnect();
            } else {
                users.push({name: username, status: 'NOT_READY', progress: 0});
                socket.emit("UPDATE_GAME", users);
                socket.broadcast.emit("UPDATE_GAME", users);
            }

        // on change ready status
        socket.on("TOGGLE_READY_STATUS", () => {
            //remove user from current room
            const currentUser = users.find(user=>user.name===username);
            const {status} = currentUser;
         
            users.find(user => user.name === username).status =  status==='READY' ? 'NOT_READY' : 'READY';

            //check if all users are active
            const notReadyUsers = users.filter(user=>user.status==='NOT_READY');
            if(!notReadyUsers.length){
                console.log("GAME START")
                socket.emit("START_GAME");
                socket.broadcast.emit("START_GAME");

                let counter = config.SECONDS_TIMER_BEFORE_START_GAME;
                const countdown = setInterval(()=>{
                  io.sockets.emit('UPDATE_COUNTDOWN', counter);
                  counter--;
                  if (counter === -1) {
                    clearInterval(countdown);
                    startGame(socket);
                  }
                }, 1000);
            }
            socket.emit("UPDATE_GAME", users);
            socket.broadcast.emit("UPDATE_GAME", users);
        });

        socket.on("UPDATE_USER_PROGRESS", (progress)=>{
            users.find(user => user.name === username).progress =  progress;
            socket.emit("UPDATE_GAME", users);
            socket.broadcast.emit("UPDATE_GAME", users);

        });

        socket.on("GAME_OVER", ()=>{
          const results = getResults(users);
          clearInterval(gameTimer);
          socket.broadcast.emit("GAME_OVER_DONE", results);
          socket.emit("GAME_OVER_DONE", results);
        });

        socket.on("REFRESH_GAME", ()=>{
          const updatedUsers = users.map(user=> ({name: user.name, status: 'NOT_READY', progress: 0}));
          users = updatedUsers;
          socket.emit("UPDATE_GAME", users);
          socket.broadcast.emit("UPDATE_GAME", users);
        });

        socket.on("disconnect", () => {
          console.log(`${username} disconnected`);
           users= users.filter(user => user.name !== username);
           socket.broadcast.emit("UPDATE_GAME", users);
        });

    });

};